clear
close all
clc
tic
load data_latih.mat

[filename,pathname] = uigetfile('*.jpg');
bacaimage = fullfile(pathname,filename);
img = imread(bacaimage);
imgresize = imresize(img,scalling_gambar);
imgGray = rgb2gray(imgresize);

gaborArray = gaborFilterBank(wave_length,orientasi,39,39);
[features,imagesi] = gaborFeatures(imgGray,gaborArray,4,4);

ubah_cell=cell2mat(imagesi);
ciri = ciriStatistik(ubah_cell(:),pilihan);

id_kelas_uji = predict(Mdl,ciri)
toc
figure(1)
k=1;
for iu=1:wave_length
    for ui=1:orientasi
        subplot(wave_length,orientasi,k);
        imshow(imagesi{k});
        k=k+1;
    end
end
figure(2)
subplot(1,2,1), imshow(imgresize); title('Gambar Resize')
subplot(1,2,2), imshow(imgGray); title('Gambar Grayscale')