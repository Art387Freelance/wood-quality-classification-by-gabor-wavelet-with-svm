clear
close all
clc
tic

%parameter preproc
scalling_gambar=0.1; %0.1 - 0.5
%pilihan ciri statistik
pilihan=6; %1-11
%parameter Gabor
wave_length=5; %4 - 7
orientasi=8; %5 - 10
%parameter SVM
jenis_SVM='Gaussian'; %Linear, Gaussian, Polynomial
%--------------------


ciri_latih = [];
target = {};
path = uigetdir; %memilih folder
jenis = dir(path); %mengambil nilai path dari folder yg dipilih
nama = {};
group = {};
iter = 1;
%perulangan berdasarkan banyak folder
for i=3:length(jenis)
    namafolder = jenis(i).name; %mengambil nama folder pada urutan ke - i
    tempatFile = [path '\' namafolder]; %menyatukan path dengan list folder (bohong atau jujur)
    filename = dir(tempatFile); %mengambil nilai path dari folder yg dipilih
    
    %perulangan berdasarkan banyaknya file mat pada folder kanal
    temp = [];
    ciri = [];
    for j=3:length(filename)
        namefile = filename(j).name; %memasukan nama file mat ke variabel
        bacaimage = [tempatFile '\' namefile]; %menyatukan file pathname dengan filename
        img = imread(bacaimage);
        imgresize = imresize(img,scalling_gambar);
        imgGray = rgb2gray(imgresize);
        
        gaborArray = gaborFilterBank(wave_length,orientasi,39,39);
        [features,imagesi] = gaborFeatures(imgGray,gaborArray,4,4);
%        figure;
%         k=1;
%         for iu=1:5
%             for ui=1:8
%                 subplot(5,8,k);
%                 imshow(imagesi{k});
%                 k=k+1;
%            
%             end
%         end
        ubah_cell=cell2mat(imagesi);
        ciri = ciriStatistik(ubah_cell(:),pilihan);
        %ciri = [mean(ubah_cell(:)) std(ubah_cell(:)) var(ubah_cell(:)) skewness(ubah_cell(:)) kurtosis(ubah_cell(:)) entropy(ubah_cell(:))];
        ciri_latih = [ciri_latih; ciri]; %menyimpan ciri latih untuk setiap file
        %         group = [group i-2]; %menyimpan nilai group target sesuai dengan masukan
        if i-2==1
            group{iter} = 'Baik';
        elseif i-2==2
            group{iter} = 'Buruk';
        elseif i-2==3
            group{iter} = 'Sedang';
        end
        iter = iter +1;
    end
end
t = templateSVM('KernelFunction', jenis_SVM,'Standardize',1);
Mdl = fitcecoc(ciri_latih,group,'Learners',t,...
    'ClassNames',{'Baik','Buruk','Sedang'});
group = group';
id_kelas_uji = predict(Mdl,ciri_latih);

jumlah = 0; %inisiasi jumlah akurasi

for l =1 : length(group) %iterasi sepanjang group
    if strcmp(id_kelas_uji{l},group{l}) %jika id_kelas_uji{l} sama dengan gr{l}
        jumlah = jumlah + 1; %maka jumlah benar ditambah
    end
end
akurasi = jumlah/length(group); %hitung akurasi
toc
save data_latih.mat ciri_latih target group Mdl scalling_gambar pilihan wave_length orientasi

