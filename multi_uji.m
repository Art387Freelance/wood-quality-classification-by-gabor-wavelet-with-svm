clear
close all
clc
tic
load data_latih.mat

ciri_uji = [];
target = {};
path = uigetdir; %memilih folder
jenis = dir(path); %mengambil nilai path dari folder yg dipilih
nama = {};
iter = 1;
group = {};

%perulangan berdasarkan banyak folder
for i=3:length(jenis)
    namafolder = jenis(i).name; %mengambil nama folder pada urutan ke - i
    tempatFile = [path '\' namafolder]; %menyatukan path dengan list folder (bohong atau jujur)
    filename = dir(tempatFile); %mengambil nilai path dari folder yg dipilih
    
    %perulangan berdasarkan banyaknya file mat pada folder kanal
    temp = [];
    ciri = [];
    for j=3:length(filename)
        namefile = filename(j).name; %memasukan nama file mat ke variabel
        bacaimage = [tempatFile '\' namefile]; %menyatukan file pathname dengan filename
        img = imread(bacaimage);
        imgresize = imresize(img,scalling_gambar);
        imgGray = rgb2gray(imgresize);
        
        gaborArray = gaborFilterBank(wave_length,orientasi,39,39);
        [features,imagesi] = gaborFeatures(imgGray,gaborArray,4,4);
        
        ubah_cell=cell2mat(imagesi);
        ciri = ciriStatistik(ubah_cell(:),pilihan);
        %ciri = [mean(ubah_cell(:)) std(ubah_cell(:)) var(ubah_cell(:)) skewness(ubah_cell(:)) kurtosis(ubah_cell(:)) entropy(ubah_cell(:))];
        ciri_uji = [ciri_uji; ciri]; %menyimpan ciri latih untuk setiap file
        %         group = [group i-2]; %menyimpan nilai group target sesuai dengan masukan
        if i-2==1
            group{iter} = 'Baik';
        elseif i-2==2
            group{iter} = 'Buruk';
        elseif i-2==3
            group{iter} = 'Sedang';
        end
        iter = iter +1;
    end
end
group = group';
id_kelas_uji = predict(Mdl,ciri_uji);

jumlah = 0; %inisiasi jumlah akurasi

for l =1 : length(group) %iterasi sepanjang group
    if strcmp(id_kelas_uji{l},group{l}) %jika id_kelas_uji{l} sama dengan gr{l}
        jumlah = jumlah + 1; %maka jumlah benar ditambah
    end
end
akurasi = jumlah/length(group); %hitung akurasi
toc
