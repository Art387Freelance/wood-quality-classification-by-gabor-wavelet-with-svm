function hasil = ciriStatistik_coba(features, jenis)
     switch jenis
         case 1
             hasil = [mean(features)];%
         case 2
             hasil = [var(features)];%
         case 3
             hasil = [std(features)];%
         case 4
             hasil = [skewness(features) ];%
         case 5
             hasil = [kurtosis(features)];%
         case 6
             hasil = [entropy(features)];%
         case 7
             hasil = [mean(features) var(features)];%
         case 8
             hasil = [mean(features) var(features) std(features)];%
         case 9
             hasil = [ var(features) std(features)  skewness(features) entropy(features)];%
         case 10
             hasil = [mean(features) var(features) std(features) kurtosis(features) entropy(features)];%
         case 11
             hasil = [mean(features) var(features) std(features) skewness(features) kurtosis(features) entropy(features)];%
         otherwise
             disp('input error')
     end
end